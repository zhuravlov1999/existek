import { AbstractControl, ValidationErrors, Validators } from "@angular/forms";
import { Observable } from "rxjs";

export class customValidator implements Validators {

    
    static minDate(control: AbstractControl): ValidationErrors | null {
        const date= new Date('1995-12-17');
        const setDate = new Date (control.value)
        console.log(setDate)
        if(setDate < date){
            console.log('error')
            return { minDate: true}
        }else {
            console.log(' no error')
            return null
        }
        
    }
    
    static minCourse(control: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> {
      
        return new Promise(resolver => {
            console.log(control.value)
            setTimeout(() => {
                if (control.value < 1) {
                    return resolver({ minCourse: true });
                } else {
                    return resolver(null);
                }
            }, 2000)
        }) 
        
    }
}