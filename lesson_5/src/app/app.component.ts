import { Component } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { customValidator } from './models/CustomValidators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  form = new FormGroup({
    name: new FormControl('', Validators.required),
    age: new FormControl('', [Validators.required, Validators.min(18)]),
    dob: new FormControl('', customValidator.minDate),
    agree: new FormControl('', Validators.requiredTrue),
    education: new FormGroup({
      uni: new FormControl(''),
      course: new FormControl('', Validators.required ,customValidator.minCourse),
      faculty: new FormControl(''),
      speciality:new FormControl(''),
    }),
    desiredTrip: new FormArray([
      new FormGroup({
        place: new FormControl(''),
        period: new FormControl('')
      })
    ])
  })

  get getEdu(){
    return  this.form.controls['education'] as FormGroup
  }

  ngOnInit(){

    this.form.get('age')?.valueChanges.subscribe(age => {
      if(age > 18){

        this.getEdu.patchValue({
          uni: 'You are now..',
          course: age,
          faculty:  '...join secret Dead Poet Society' 
        })
      }
    })
  }



  onSubmit(){
    console.log(this.form)
  }
}
