
interface Pupil {
    id: number,
    name: string,
    age: number,
    topic?: string,
    schedule?: [number, string]
}
const roy: Pupil = {
    id: 1,
    name: 'Roy',
    age: 25,
    schedule: [12, 'Wednesday']
}

interface Teacher extends Pupil {
    canTeach: boolean
} 
const yuri: Teacher = {
    id: 2,
    name: 'Roy',
    age: 23,
    canTeach: true
} 

class teacher implements Teacher {
    id: number;
    name: string;
    age: number;
    canTeach: boolean;

    constructor (teacherid:number, teachername: string, teacherage: number, teachercanteach: boolean) {
        this.id = teacherid;
        this.name =  teachername;
        this.age = teacherage;
        this.canTeach = teachercanteach;
    }

    CanStartTeaching(): void{
        if(this.canTeach){
            console.log('You have an appropriate education, you can start teaching!😎')
        }
    }
}

const anton = new teacher(3, 'Anton', 23, true)
anton.CanStartTeaching()

class student {
    private id: number;
    name: string;
    protected yearOfStudy: number;
    readonly greatPointAverage: number;
    graduationStatus: string;
    
    constructor (studId: number, studName: string, studYearOfStudy: number, studGreatPointAverage: number){
        this.id = studId;
        this.name = studName;
        this.yearOfStudy = studYearOfStudy; 
        this.greatPointAverage = studGreatPointAverage;
        this.graduationStatus = this.canGraduate();
    }

    protected canGraduate(): string{
        if(this.greatPointAverage > 3 && this.yearOfStudy === 4){
            return 'This student can graduate!😤😤😤'
        } else {
            return "We're so sorry😢"
        }
    }
}
const michael = new student(4, 'Michael', 4, 4);


type area = string;
type articles = boolean | number;

class phDStudent extends student {
    subjectArea: area;
    currentProj: string;
    hasArticles: articles;

    canHavePhD():void{
        console.log(this.canGraduate)
    }
}

enum ModalVerbs {
    can = 'can',
    may = 'may',
    must = 'must',
    should = 'should'
}
enum UserResponse {
    No,
    Yes
}

const printModal = function(verb :ModalVerbs, response: UserResponse): void {
    console.log(`Do you know this verb:"${verb}"?`)
    if(response){
        console.log ('Nice👌')
    } else 'Well...'
} 
printModal(ModalVerbs.must, UserResponse.Yes);

function generic<T>(param: T):T{
    return param
}
generic<string>('stringsss')