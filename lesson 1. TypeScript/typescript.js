var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var roy = {
    id: 1,
    name: 'Roy',
    age: 25,
    schedule: [12, 'Wednesday']
};
var yuri = {
    id: 2,
    name: 'Roy',
    age: 23,
    canTeach: true
};
var teacher = /** @class */ (function () {
    function teacher(teacherid, teachername, teacherage, teachercanteach) {
        this.id = teacherid;
        this.name = teachername;
        this.age = teacherage;
        this.canTeach = teachercanteach;
    }
    teacher.prototype.CanStartTeaching = function () {
        if (this.canTeach) {
            console.log('You have an appropriate education, you can start teaching!😎');
        }
    };
    return teacher;
}());
var anton = new teacher(3, 'Anton', 23, true);
anton.CanStartTeaching();
var student = /** @class */ (function () {
    function student(studId, studName, studYearOfStudy, studGreatPointAverage) {
        this.id = studId;
        this.name = studName;
        this.yearOfStudy = studYearOfStudy;
        this.greatPointAverage = studGreatPointAverage;
        this.graduationStatus = this.canGraduate();
    }
    student.prototype.canGraduate = function () {
        if (this.greatPointAverage > 3 && this.yearOfStudy === 4) {
            return 'This student can graduate!😤😤😤';
        }
        else {
            return "We're so sorry😢";
        }
    };
    return student;
}());
var michael = new student(4, 'Michael', 4, 4);
var phDStudent = /** @class */ (function (_super) {
    __extends(phDStudent, _super);
    function phDStudent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    phDStudent.prototype.canHavePhD = function () {
        console.log(this.canGraduate);
    };
    return phDStudent;
}(student));
var ModalVerbs;
(function (ModalVerbs) {
    ModalVerbs["can"] = "can";
    ModalVerbs["may"] = "may";
    ModalVerbs["must"] = "must";
    ModalVerbs["should"] = "should";
})(ModalVerbs || (ModalVerbs = {}));
var UserResponse;
(function (UserResponse) {
    UserResponse[UserResponse["No"] = 0] = "No";
    UserResponse[UserResponse["Yes"] = 1] = "Yes";
})(UserResponse || (UserResponse = {}));
var printModal = function (verb, response) {
    console.log("Do you know this verb:\"".concat(verb, "\"?"));
    if (response) {
        console.log('Nice👌');
    }
    else
        'Well...';
};
printModal(ModalVerbs.must, UserResponse.Yes);
function generic(param) {
    return param;
}
generic('stringsss');
