import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'isAdult'
})
export class IsAdultPipe implements PipeTransform {

  transform(value: number): boolean{
    return value > 18 ? true : false;
  }

}
