import { ElementRef, Pipe, PipeTransform, ViewChild } from '@angular/core';

@Pipe({
  name: 'legalAgeFilter'
})
export class LegalAgeFilterPipe implements PipeTransform {
  transform(value: any[], filterbyLegalAge: boolean): any {
    if(!filterbyLegalAge){
      return value
    }
    return value.filter(user => user.dob.age > 30);
  }

}
