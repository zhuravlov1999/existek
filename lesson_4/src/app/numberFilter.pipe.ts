import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'numberFilter'
})
export class NumberFilterPipe implements PipeTransform {

  transform(value: any[], filterByAge: number): any[] {
    if(!filterByAge){
      return value;
    }
    return value.filter( user => {
      return Object.values(user.dob).includes(filterByAge)
    } );
  }

}
