import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { FilterPipe } from './filter.pipe';
import { NumberFilterPipe } from './numberFilter.pipe';
import { LocationFilterPipe } from './locationFilter.pipe';
import { DateFilterPipe } from './dateLessThanFilter.pipe';
import { DateBiggerThanFilterPipe } from './dateBiggerThanFilter.pipe';
import { IsAdultPipe } from './isAdult.pipe';
import { LegalAgeFilterPipe } from './legalAgeFilter.pipe';

@NgModule({
  declarations: [						
    AppComponent,
    FilterPipe,
      NumberFilterPipe,
      LocationFilterPipe,
      DateFilterPipe,
      DateBiggerThanFilterPipe,
      IsAdultPipe,
      LegalAgeFilterPipe
   ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
