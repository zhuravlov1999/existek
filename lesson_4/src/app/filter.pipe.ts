import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {
  userArr!:any
  transform(value: any[], filterByName:string): any[] {
    if(!filterByName.trim()){
      return value
    }

    return value.filter(user => {
      this.userArr = Object.values(user.name)
      return this.userArr.some((str:any) => str.includes(filterByName))
    });
  }

}
