import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dateBiggerThanFilter'
})
export class DateBiggerThanFilterPipe implements PipeTransform {

  userDate!: any
  filterDate!: Date
  transform(value: any[], filterByDate:number | Date): any[] {

    if(!filterByDate){
      return value
    }

    return value.filter(user => {
      this.userDate = new Date(user.dob.date.slice(0, 10))
      this.filterDate = new Date(filterByDate)
      return this.filterDate.getTime() < this.userDate.getTime()
    });
  }
}
