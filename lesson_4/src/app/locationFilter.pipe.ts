import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'locationFilter'
})
export class LocationFilterPipe implements PipeTransform {
  userArr!: any
  transform(value: any[], filterByLocation:string): any[] {

    if(!filterByLocation.trim()){
      return value
    }

    return value.filter(user => {
      this.userArr = Object.values(user.location)
      return user.location.city.includes(filterByLocation)
    });
    }

}
