import { Component, ElementRef, Input, ViewChild } from '@angular/core';
import {HttpClient} from '@angular/common/http';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  resp: any;
  users:any[] = [];
  @ViewChild('dateLessRef') dateLessRef!:ElementRef;
  @ViewChild('dateBiggerRef') dateBiggerRef!:ElementRef;

  constructor(private http: HttpClient ){}
  ngOnInit(){
    this.getUsers() 
  }
  
  filterbyName = '';
  filterbyLocation = '';
  filterbyAge!: number;
  filterbyLessD = this.dateLessRef?.nativeElement.valueAsDate;
  filterbyBiggerD = this.dateBiggerRef?.nativeElement.valueAsDate;
  filterbyLegalAge = false;
  
  getUsers(){
    for(let i = 0; i < 4; i++){
      this.http.get('https://randomuser.me/api/').subscribe(response => {
      this.resp = response;
      this.users.push(...this.resp.results)
      })
    }
  }

  checkboxVal(event:any){
    this.filterbyLegalAge = event.target.checked;
  }
  widthExp = {'width': '40px'}
  borderExp = '1px solid';

}
