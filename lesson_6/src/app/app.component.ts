import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, ViewChild } from '@angular/core';
import { User } from './models/user';
import { ControlService } from './services/control.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  test = 'red';
  users: User[] = [];
  @ViewChild('input') input!: ElementRef;
  
  constructor(private http: HttpClient, public controlService: ControlService){}

  ngOnInit(){
    this.getUsers(4)
  }

  getUsers(amount: number){
    for(let id = 1; id <= amount; id++){
      this.http.get('https://jsonplaceholder.typicode.com/users/' + id).subscribe((response: any) => {
        console.log(response)
        this.users.push(response)
      })
    }
  }
  addUser(){
    this.controlService.addUser(this.userHardcode).subscribe((response: any) => {
      this.users.push(response)
    })
  }
  updUsername(user: User, event: Event){

    const inputNameValue = (event.target as HTMLOutputElement).value ;
    const updData = {...user, username: inputNameValue}

    this.controlService.updUsername(user.id, updData).subscribe((response: any) => {
      const userIndex = this.users.indexOf(user);
      console.log(this.users[userIndex])
      this.users[userIndex] = response
    })

  }

  deleteNode(user: any){
    this.controlService.deleteNode(user.id).subscribe(response => {
      const userIndex = this.users.indexOf(user);
      this.users.splice(userIndex, 1)
    })
  }
  
  userHardcode: User = {
    address: {street: 'Douglas Extension', suite: 'Suite 847', city: 'McKenziehaven', zipcode: '59590-4157'},
    company: {name: 'Romaguera-Jacobson', catchPhrase: 'Face to face bifurcated interface', bs: 'e-enable strategic applications'},
    email: "Nathan@yesenia.net",
    id: 3,
    name: "Clementine Bauch",
    phone: "1-463-123-4447",
    username: "Samantha",
    website: "ramiro.info",
  }
}
