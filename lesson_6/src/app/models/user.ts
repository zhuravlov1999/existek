export interface User {
    id: number
    name: string
    phone: string
    username: string
    website: string
    address?: any
    company?: any
    email: string
}
