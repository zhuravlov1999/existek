import { Directive, ElementRef, Input, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appHighlight]'
})


export class HighlightDirective {
  

  @Input()
  appHighlight!: number
  
  constructor(private el: ElementRef, private render: Renderer2) {
    
  }
  
  ngOnInit(){
    if(this.appHighlight > 3){
      this.render.setStyle(this.el.nativeElement, 'background-color', 'red')
    }
  }
  
}
