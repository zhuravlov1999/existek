import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class ControlService {

  constructor(private http: HttpClient) { }

  addUser(hardcode: User){
    return this.http.post('https://jsonplaceholder.typicode.com/users/',  hardcode)
  }
  updUsername(id: number, updData: any){
    return this.http.put('https://jsonplaceholder.typicode.com/users/' + id, updData)
  }
  deleteNode(id: number){
    return this.http.delete('https://jsonplaceholder.typicode.com/users/' + id)
  }

}
