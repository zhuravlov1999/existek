export interface User {
    id: number,
    name: string,
    emails: string[],
    passwords: string[],
    projects: string[]
}
