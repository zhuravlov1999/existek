import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot, Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-project.details',
  templateUrl: './project.details.component.html',
  styleUrls: ['./project.details.component.scss']
})
export class ProjectDetailsComponent implements OnInit {
  users!: any;
  projectQueryParam!: string;
  constructor(private userService: UserService, private route: ActivatedRoute, private router: Router) {
    // this.projectQueryParam = routeSnapshot.queryParams['myParam']
  }
  ngOnInit(): void {
    this.route.data.subscribe(data => this.users = data['project'])  
  }
  
  onUserClick(user: any){
    this.router.navigate(['/projects', 'project', user.id])
  }
  

}
