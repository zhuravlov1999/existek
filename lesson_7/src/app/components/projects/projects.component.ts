
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { ProjectDetailsComponent } from './project.details/project.details.component';

const routes = [
  {
    path: ':id',
    component: ProjectDetailsComponent,
    data: {
      projects: 'project'
    }
  }
]

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent {
  projectIndexer = 0;
  projects = [
    {
      id: this.projectIndexer++,
      name: 'Multi-Page Responsive Website',
    },
    {
      id: this.projectIndexer++,
      name: 'Small JavaScript Game Desing',
    },
    {
      id: this.projectIndexer++,
      name: 'Chess Game',
    },
  ]

  constructor(private router: Router) {
     
  }
  onProjectClick(project: any){

    this.router.navigate(['/projects',  project.id], {queryParams: {myParam: project.name}})
    
  }




}
