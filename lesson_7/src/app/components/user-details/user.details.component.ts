import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user-details',
  templateUrl: './user.details.component.html',
  styleUrls: ['./user.details.component.scss']
})
export class UserDetailsComponent implements OnInit {

  userId!: number;
  user!: any
  constructor(private route: ActivatedRoute, private userService: UserService) {}

  ngOnInit() {
    this.route.params.subscribe(param => this.userId = +param['id']);
    this.userService.getUserById(this.userId).subscribe(user => this.user = user);
  }

}
