import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProjectsComponent } from './components/projects/projects.component';
import { SponsorsComponent } from './components/sponsors/sponsors.component';
import { ProjectDetailsComponent } from './components/projects/project.details/project.details.component';
import { UserDetailsComponent } from './components/user-details/user.details.component';

@NgModule({
  declarations: [
    AppComponent,
    ProjectsComponent,
    SponsorsComponent,
    ProjectDetailsComponent,
    UserDetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
