import { Component } from '@angular/core';
import { User } from './models/user';
import { LoginService } from './services/login.service';
import { UserService } from './services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  users: User[] = [];
  
  
  constructor(private userService: UserService, public loginService: LoginService){}

  ngOnInit(){
    this.userService.getUsers().subscribe((users:any) => {
      this.users = users;
    })
  }

}
