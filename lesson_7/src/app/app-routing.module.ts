import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { ProjectDetailsComponent } from './components/projects/project.details/project.details.component';
import { ProjectsComponent } from './components/projects/projects.component';
import { SponsorsComponent } from './components/sponsors/sponsors.component';
import { UserDetailsComponent } from './components/user-details/user.details.component';
import { LoginGuardService } from './services/loginGuard.service';
import { ProjectResolveService } from './services/project-resolve.service';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'projects',
    component: ProjectsComponent,
    children: [
      {
        path: ':projectName',
        component: ProjectDetailsComponent,
        resolve: {
          project: ProjectResolveService
        },
        
      },
      {
        path: ':projectName/:id',
        component: UserDetailsComponent
      }
    ],
    canActivate: [LoginGuardService]
  },
  {
    path: 'sponsors',
    component: SponsorsComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
