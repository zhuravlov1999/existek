import { Injectable } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { map, Observable } from 'rxjs';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class ProjectResolveService {
  
  constructor(private userService: UserService, private route: ActivatedRoute) { }

  ngOnInit(){
    
    console.log(this.route.queryParams)
  }

  resolve(route: ActivatedRouteSnapshot): Observable<any>  {
    const projectName = route.queryParams['myParam']
    return this.userService.getUserByProj(projectName)
  }

}
