import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  idCount = 0;

  users: User[] = [
    {
      id: this.idCount++,
      name: 'John',
      emails: ['johndog@gmail.com'],
      passwords: ['123', 'elfo', 'elfoe'],
      projects: ['Multi-Page Responsive Website']
    },
    {
      id: this.idCount++,
      name: 'Ludvig',
      emails: ['Ludvigdog@gmail.com'],
      passwords: ['123', 'elfo', 'elfoe'],
      projects: ['Small JavaScript Game Desing']
    },
    {
      id: this.idCount++,
      name: 'James',
      emails: ['Jamesdog@gmail.com'],
      passwords: ['123', 'elfo', 'elfoe'],
      projects: ['Chess Game']
    },
    {
      id: this.idCount++,
      name: 'Sunny',
      emails: ['Sunnydog@gmail.com'],
      passwords: ['123', 'elfo', 'elfoe'],
      projects: ['Multi-Page Responsive Website', 'Chess Game']
    },
  ];

  getUsers(){
    return of(this.users)
  };

  getUserByProj(project: string){
    const user = this.users.filter(user => user.projects.includes(project))
    return of(user);
  }
  getUserById(id: number){
    const user = this.users.find(user => user.id === id)
    return of(user);
  }
}