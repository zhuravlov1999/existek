import { Component } from '@angular/core';

@Component({
  selector: 'app-grocery',
  templateUrl: './grocery.component.html',
  styleUrls: ['./grocery.component.css']
})
export class GroceryComponent {
  
  label = 'Grocery List:'
  item: string = '';
  groceries: string[] = [];
  
  addItem () {
    this.groceries.push(this.item)
    this.item = '';
  }
  

}
