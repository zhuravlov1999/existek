import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'this is a simple rendering';

  binding = 'this is binding';
  twoWayBinding = 'this is two way binding';


  go:string | boolean = 'sad kitty';
  alternatives = 'Choose an answer:';
  active = true;
  legend = '';

  goShopping(a:string):void{
    if(+a){
      this.go = true;
      this.active = false;
      this.legend = 'Groceries';
    } else {
      this.go = false;
      this.active = false;
      this.legend = 'Why no?';
    }
  }

  widthExpr:number = 400;
  heightExpr:string = 'fit-content';
}
