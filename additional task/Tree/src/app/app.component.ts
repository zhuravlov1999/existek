import { Component } from '@angular/core';


interface INode {
  id:number;
  name:string;
  children?:INode[];
}



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  idCounter = 0;
  value = '';
  text = '';
  
  nodes:INode[] = []
  selectedTreeItem:any = '';

  inputEditActive = false;
  inputAddChildActive = false;
  editButton(item:any){
    this.selectedTreeItem = item;
    this.inputEditActive = !this.inputEditActive;
  }
  
  addChildButton(node:any){
    this.selectedTreeItem = node;
    this.inputAddChildActive = !this.inputAddChildActive;
  }


  editItem(event:Event, item:any):void{
    this.value = (<HTMLOutputElement>event.target).value
    item.name = this.value
    this.inputEditActive = false;
  }

  addItem(event: Event):void{
    this.value = (<HTMLOutputElement>event.target).value
    this.nodes.push({id: this.idCounter++, name: this.value})
    this.text = '';
  }

  deleteItem(node:any, item:any):void{
    node.forEach((currNode:any,index:number)=>{
      if(currNode==item) node.splice(index,1);
    })
  }


  addChild(event:Event, item: any){
    this.value = (<HTMLOutputElement>event.target).value
    if(item.children === undefined){
      item.children = [{id: this.idCounter++, name: this.value}]
      console.log(item.children)
    } else {
      console.log(item)
      item.children.push({id: this.idCounter++, name: this.value})
    }
    this.inputAddChildActive = false;
  }

  

}
