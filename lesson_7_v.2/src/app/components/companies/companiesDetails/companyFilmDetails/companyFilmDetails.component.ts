import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Company } from 'src/app/models/company';
import { CompanyService } from 'src/app/services/company.service';

@Component({
  selector: 'app-companyFilmDetails',
  templateUrl: './companyFilmDetails.component.html',
  styleUrls: ['./companyFilmDetails.component.scss']
})
export class CompanyFilmDetailsComponent implements OnInit {
  film!: any;
  params!: any

  constructor(private companyService: CompanyService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.route.params.subscribe( param => this.params = param)
    this.companyService.getFilm(this.params).subscribe(film => this.film = film)
  }

  
  back(){
    this.router.navigate(['/companies', {outlets: {list: ['companiesList'], details: [this.params.id]}}])
  }

  ngAfterViewInit() {
    let top = document.getElementById('top');
    if (top !== null) {
      top.scrollIntoView();
      top = null;
    }
  }

}
