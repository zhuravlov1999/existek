import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Company } from 'src/app/models/company';
import { CompanyService } from 'src/app/services/company.service';

@Component({
  selector: 'app-companiesDetails',
  templateUrl: './companiesDetails.component.html',
  styleUrls: ['./companiesDetails.component.scss']
})
export class CompaniesDetailsComponent implements OnInit {
  
  company: Company | undefined;

  constructor(private companiesService: CompanyService, private route: ActivatedRoute ) { }

  ngOnInit() {
    this.route.params.subscribe( param => {
      this.companiesService.getCompany(+param['id']).subscribe(company => {this.company = company})
    })
    
  }

}
