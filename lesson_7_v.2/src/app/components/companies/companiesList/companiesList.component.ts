import { Component, OnInit } from '@angular/core';
import { Company } from 'src/app/models/company';
import { CompanyService } from 'src/app/services/company.service';

@Component({
  selector: 'app-companiesList',
  templateUrl: './companiesList.component.html',
  styleUrls: ['./companiesList.component.scss']
})
export class CompaniesListComponent implements OnInit {

  companies: Company[] = [];

  constructor(private companiesService: CompanyService) { }

  ngOnInit() {
    this.companiesService.getCompanies().subscribe(companies => this.companies = companies)
  }

}
