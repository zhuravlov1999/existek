import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Scenarist } from 'src/app/models/scenarist';
import { ScenaristService } from 'src/app/services/scenarist.service';

@Component({
  selector: 'app-scenarist',
  templateUrl: './scenarists.component.html',
  styleUrls: ['./scenarists.component.scss']
})
export class ScenaristsComponent implements OnInit {

  scenarists: Scenarist[] = [];

  constructor(private scenaristService: ScenaristService, private router: Router) { }

  ngOnInit() {
    this.scenaristService.getScenarists().subscribe(scenarists => this.scenarists = scenarists)
  }

  

}
