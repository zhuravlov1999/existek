import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Scenarist } from 'src/app/models/scenarist';
import { ScenaristService } from 'src/app/services/scenarist.service';

@Component({
  selector: 'app-scenaristsList',
  templateUrl: './scenaristsList.component.html',
  styleUrls: ['./scenaristsList.component.scss']
})
export class ScenaristsListComponent implements OnInit {
  scenarists: Scenarist[] = []

  constructor(private scenaristService: ScenaristService, private router: Router) { }

  ngOnInit() {
    this.scenaristService.getScenarists().subscribe(scenarists => this.scenarists = scenarists)
  }
  navigate(id:number){
    this.router.navigate(['/scenarists', {outlets: {list: ['scenaristList'], details: [id]}}])
    
  }

}
