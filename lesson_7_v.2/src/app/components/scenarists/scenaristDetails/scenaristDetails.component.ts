import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ScenaristService } from 'src/app/services/scenarist.service';

@Component({
  selector: 'app-scenaristDetails',
  templateUrl: './scenaristDetails.component.html',
  styleUrls: ['./scenaristDetails.component.scss']
})
export class ScenaristDetailsComponent implements OnInit {
  scenarist!: any

  constructor(private scenarisService: ScenaristService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.route.params.subscribe(id => {
      console.log(id)
      this.scenarisService.getScenarist(+id['id']).subscribe(scenarist => this.scenarist = scenarist )
    })
    console.log(this.scenarist)
  }
  navigate(id:number, name:string){
    this.router.navigate(['/scenarists', {outlets: {list: ['scenaristList'], details: [id, name]}}])
  }
  

}
