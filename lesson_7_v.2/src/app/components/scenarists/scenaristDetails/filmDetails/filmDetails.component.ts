import { Component, OnChanges, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Scenarist } from 'src/app/models/scenarist';
import { ScenaristService } from 'src/app/services/scenarist.service';

@Component({
  selector: 'app-filmDetails',
  templateUrl: './filmDetails.component.html',
  styleUrls: ['./filmDetails.component.scss']
})
export class FilmDetailsComponent implements OnInit {

  film!: any;
  params!: any;
  constructor(private scenaristService: ScenaristService, private route: ActivatedRoute, private router: Router) {
    
  }

  ngOnInit() {
    this.route.params.subscribe( param => this.params = param)
    this.scenaristService.getFilm(this.params).subscribe(film => this.film = film)
  }
  
  back(){
    this.router.navigate(['/scenarists', {outlets: {list: ['scenaristList'], details: [this.params.id]}}])
  }

  ngAfterViewInit() {
    let top = document.getElementById('top');
    if (top !== null) {
      top.scrollIntoView();
      top = null;
    }
  }
  

}
