export interface Scenarist {
    id: number;
    name: string;
    recomendation: string;
    filmScripts?: any[];
}
