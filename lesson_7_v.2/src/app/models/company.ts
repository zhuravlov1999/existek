export interface Company {
  id: number,
  name: string,
  description: string,
  address: {
    country: string,
    state: string,
    city: string
  },
  films: any[]
}
