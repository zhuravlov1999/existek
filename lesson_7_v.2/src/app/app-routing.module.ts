import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FilmDetailsComponent } from './components/scenarists/scenaristDetails/filmDetails/filmDetails.component';
import { HomeComponent } from './components/home/home.component';
import { ScenaristDetailsComponent } from './components/scenarists/scenaristDetails/scenaristDetails.component';
import { ScenaristsComponent } from './components/scenarists/scenarists.component';
import { ScenaristsListComponent } from './components/scenarists/scenaristsList/scenaristsList.component';
import { CompaniesComponent } from './components/companies/companies.component';
import { CompaniesListComponent } from './components/companies/companiesList/companiesList.component';
import { CompaniesDetailsComponent } from './components/companies/companiesDetails/companiesDetails.component';
import { CompanyFilmDetailsComponent } from './components/companies/companiesDetails/companyFilmDetails/companyFilmDetails.component';
import { AuthGuardService } from './services/authGuard.service';
import { ChildrenGuardService } from './services/childrenGuard.service';
import { NotFoundComponent } from './components/notFound/notFound.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  
  {
    path: 'scenarists',
    component: ScenaristsComponent,
    canActivateChild: [ChildrenGuardService],
    children: [
      {
        path: 'scenaristList',
        component: ScenaristsListComponent,
        outlet: 'list'
      },
      {
        path: ':id',
        component: ScenaristDetailsComponent,
        outlet: 'details',
      },
      {
        path: ':id/:filmDetails',
        component: FilmDetailsComponent,
        outlet: 'details'
      }
    ],
    canActivate: [AuthGuardService]
  },
  {
    path: 'companies',
    component: CompaniesComponent,
    children: [
      {
        path: 'companiesList',
        component: CompaniesListComponent,
        outlet: 'list'
      },
      {
        path: ':id',
        component: CompaniesDetailsComponent,
        outlet: 'details',
      },
      {
        path: ':id/:companyFilmDetails',
        component: CompanyFilmDetailsComponent,
        outlet: 'details'
      }
    ],
    canActivateChild: [ChildrenGuardService],
    canActivate: [AuthGuardService]
  },
  {
    path: '**',
    redirectTo: 'notFound'
  },
  {
    path: 'notFound',
    component: NotFoundComponent
  }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {scrollOffset: [0, 0]})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
