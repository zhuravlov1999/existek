import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { Company } from '../models/company';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {


  companyIndexer: number = 0;

  constructor() { }

  companies: Company[] = [
    {
      id: this.companyIndexer++,
      name: 'Holywood Pictures',
      description: 'Hollywood Pictures was an American film production and distribution company and a division of The Walt Disney Studios, a business segment of The Walt Disney Company, While then-Disney CEO Michael Eisner at first intended Hollywood Pictures to be a full-fledged studio, like Touchstone Pictures. In later years its operations had been scaled back and its management was merged with that of the flagship Walt Disney Pictures studio. Its most profitable film was M. Night Shyamalan\'s film The Sixth Sense, which grossed over $600 million worldwide upon its 1999 release.[1]',
      address: {
        country: 'the United States of America',
        state: 'California',
        city: 'Hollywood'
      },
      films: [
        {
          name: 'The Invisible',
          genre: ['Supernatural thriller', 'Teen'],
          realeaseDate: this.releaseDate('April 27, 2007'),
          review: 'High school senior Nick Powell plans to skip his graduation and fly to London for a writing program, despite the plan of his controlling mother, Diane. His mother pressures him to succeed and is emotionally distant. Nick\'s best friend, Pete Egan, confides in him that he is bullied by Annie Newton, a troubled teen. Nick attempts to step in on one such occasion, only for it to escalate into a physical confrontation. Annie\'s closest friends are violent thieves, and her boyfriend, Marcus, is on parole for similar violations.'
        },
        {
          name: 'Primeval',
          genre: ['Action-adventure', 'Horror'],
          realeaseDate: this.releaseDate('January 12, 2007'),
          review: 'In Burundi, a British forensic anthropologist is examining the corpses in a mass grave, claiming they were all killed in an identical manner. When the woman digs her shovel into what she believes is another grave, an unseen creature attacks and violently drags her into the river. The UN soldiers accompanying her fire into the water, but only her mangled corpse floats to the surface - before being devoured.'
        },
      ]
    },
    {
      id: this.companyIndexer++,
      name: 'Universal Pictures',
      description: 'Universal Pictures is an American film production and distribution company owned by Comcast through the NBCUniversal Film and Entertainment division of NBCUniversal. Founded in 1912 by Carl Laemmle, Mark Dintenfass, Charles O. Baumann, Adam Kessel, Pat Powers, William Swanson, David Horsley, Robert H. Cochrane, and Jules Brulatour, Universal is the oldest surviving film studio in the United States; the world\'s fifth oldest after Gaumont, Pathé, Titanus, and Nordisk Film; and the oldest member of Hollywood\'s "Big Five" studios in terms of the overall film market. Its studios are located in Universal City, California, and its corporate offices are located in New York City. In 1962, the studio was acquired by MCA, which was re-launched as NBCUniversal in 2004. Universal Pictures is a member of the Motion Picture Association (MPA), and was one of the "Little Three" majors during Hollywood\'s golden age.[4]',
      address: {
        country: 'the United States of America',
        state: 'California',
        city: 'Universal city'
      },
      films: [
        {
          name: 'The Addams Family',
          genre: ['Black comedy'],
          realeaseDate: this.releaseDate('October 11, 2019'),
          review: 'An angry mob that disapproves of macabre nature disrupts the midnight wedding ceremony of Gomez and Morticia and drives them away with the rest of the Addams clan. During the chaos, Grandmama buys Fester time to evacuate Gomez, Morticia, and Thing. Gomez and Morticia decide to move to New Jersey, a place "no one would be caught dead in." There, Gomez, Morticia, and Thing find their "perfect" home in an abandoned asylum on a hill. They meet Lurch, an escaped mental patient whom they hit when Thing was driving their car, and immediately recruit him as their butler.'
        },
        {
          name: 'The Boss Baby',
          genre: ['Comedy', 'Family'],
          realeaseDate: this.releaseDate('March 31, 2017'),
          review: 'The franchise\'s inciting incident is the birth of the titular Boss Baby, later known as Ted, who arrives at 7-year-old Tim\'s home in a taxi, wearing a suit and carrying a briefcase, the series\' timeline spanning to focus on a now 40-year-old Tim\'s daughters, the 7-year-old Tabitha and new Boss Baby Tina.'
        },
      ]
    },
    {
      id: this.companyIndexer++,
      name: 'Columbia Pictures',
      description: 'Columbia Pictures Industries, Inc. is an American film production studio that is a member of the Sony Pictures Motion Picture Group,[2] a division of Sony Entertainment\'s Sony Pictures Entertainment, which is one of the Big Five studios and a subsidiary of the multinational conglomerate Sony',
      address: {
        country: 'the United States of America',
        state: 'California',
        city: 'Hollywood'
      },
      films: [
        {
          name: 'The Amazing Spider-Man',
          genre: ['superhero film'],
          realeaseDate: this.releaseDate('July 3, 2012'),
          review: 'A young Peter Parker / Spider-Man discovers that his father Richard Parker\'s study has been burgled. Peter\'s parents gather hidden documents, take Peter to the home of his Aunt May and Uncle Ben, then mysteriously depart. Years later, a teenage Peter attends Midtown Science High School; he is intelligent but socially awkward and often bullied. He also has a crush on Gwen Stacy, who returns his feelings. Peter learns his father worked with scientist Dr. Curt Connors at Oscorp in the field of cross-species genetics. He sneaks into Oscorp, where he is bitten by a genetically modified spider. He then discovers he has developed spider-like abilities, such as super-strength, sharp senses, agility, and speed.'
        }
      ]
    },
    {
      id: this.companyIndexer++,
      name: 'Walt Disney Pictures',
      description: 'Walt Disney Pictures is an American film production studio and subsidiary of Walt Disney Studios, which is owned by The Walt Disney Company. The studio is the flagship producer of live-action feature films within the Walt Disney Studios unit, and is based at the Walt Disney Studios in Burbank, California. Animated films produced by Walt Disney Animation Studios and Pixar Animation Studios are also released under the studio banner. Walt Disney Studios Motion Pictures distributes and markets the films produced by Walt Disney Pictures. Disney began producing live-action films in the 1950s, under the company\'s all-encompassing name, Walt Disney Productions. The live-action division took on its current incorporated name of Walt Disney Pictures in 1983, when Disney reorganized its entire studio division; which included the separation from the feature animation division and the subsequent creation of Touchstone Pictures; a sister division responsible for producing mature films not suitable for release through Walt Disney Pictures. At the end of that decade, combined with Touchstone\'s output, Walt Disney Pictures elevated Walt Disney Studios as one of Hollywood\'s major film studios. Walt Disney Pictures is currently one of five live-action film studios within the Walt Disney Studios, the others being 20th Century Studios, Marvel Studios, Lucasfilm, and Searchlight Pictures. The 2019 remake of The Lion King is the studio\'s highest-grossing film worldwide with $1.6 billion,[4] and Pirates of the Caribbean is the studio\'s most successful film series, with five films earning a total of over $4.5 billion in worldwide box office gross.',
      address: {
        country: 'the United States of America',
        state: 'California',
        city: 'Burbank'
      },
      films: [
        {
          name: 'Ice Age',
          genre: ['Comedy', 'Family'],
          realeaseDate: this.releaseDate('April 27, 2007'),
          review: 'Ice Age is an American media franchise centering on a group of mammals surviving the Paleolithic ice age. Sid, a goofy but good-natured sloth, is left behind by his family and the herds of mammals journeying to the south. He meets Manny, a cynical and loner woolly mammoth who is travelling to the north, and decides to follow him, much to Manny\'s disdain. Along the way, the pair come across a woman who has jumped down the nearby waterfall in a desperate bid to protect her child after their camp was attacked by saber-toothed tigers.'
        },
        {
          name: 'Zootopia',
          genre: ['Comedy', 'Family'],
          realeaseDate: this.releaseDate('April 27, 2007'),
          review: 'Rabbit Judy Hopps from rural Bunnyburrow fulfills her childhood dream of becoming a police officer in urban Zootopia. Despite being the academy valedictorian, Judy is delegated to parking duty by Chief Bogo, who fails to recognize her talent. On her first day on the job, Judy is hustled by a con artist fox duo, Nick Wilde and Finnick.'
        },
      ]
    },
  ]

  getCompanies(){
    return of(this.companies);
  }

  getCompany(id:number){
    const company = this.companies.find(company => company.id === id) 
    console.log(company)
    return of(company)
  }

  getFilm(i:any){
    const company = this.companies.find( company => company.id === +i.id);
    const film = company?.films?.filter( film => film.name === i.companyFilmDetails )
    return of(...film)
  }

  releaseDate(date: string){
    return new Date(date);
  }
}
