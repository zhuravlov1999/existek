import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { Scenarist } from '../models/scenarist';

@Injectable({
  providedIn: 'root'
})
export class ScenaristService {
  indexer = 0;

  constructor() { }

  scenarists: Scenarist[] = [
    {
      id: this.indexer++,
      name: 'Chloé Zhao',
      recomendation: "The newest and most up-and-coming writer to appear here is recent Best Director winner Chloé Zhao. While Zhao's win spells nothing but good things for Hollywood's acknowledgment of women and POC filmmakers, it is also an incredible boon to what is sure to be a long and storied career for the filmmaker. Acknowledged mostly for their directing work on Nomadland, Zhao's achievements as a screenwriter are equally impressive. Their nuanced and wise-beyond-their-years writing is indicative of long-awaited changes to modern Hollywood that are finally beginning to take root, the inspiring story writes itself.",
      filmScripts: [
        {
          name: 'Nomadland',
          genre: ['Drama', 'Western'],
          synopsis: 'In 2011, Fern loses her job after the US Gypsum plant in Empire, Nevada shuts down; she worked there for years along with her husband who recently died. Fern decides to sell most of her belongings and purchase a van to live in and travel the country searching for work. She takes a seasonal job at an Amazon fulfillment center through the winter. A friend and co-worker named Linda invites Fern to visit a desert winter gathering in Arizona organized by Bob Wells, which provides a support system and community for fellow nomads. Fern initially declines the offer, but changes her mind as the weather turns south and she struggles to find work in the area. At the gathering, Fern meets fellow nomads and learns basic survival and self-sufficiency skills for the road.'
        },
        {
          name: 'The Rider',
          genre: ['Drama', 'Western'],
          synopsis: 'After suffering a near fatal head injury, a young cowboy undertakes a search for new identity and what it means to be a man in the heartland of America.'
        }
      ]
      
    },
    {
      id: this.indexer++,
      name: 'Frank Darabont',
      recomendation: "The story that a Frank Darabont biopic could follow is the incredibly inspiring story of how he got to direct his own script for his feature film debut. That film would go on to be The Shawshank Redemption, widely regarded as one of the finest films ever made. Darabont made a short film adaption of a Stephen King story, which led to an unofficial deal and lifelong friendship between the young filmmaker and the novelist. Darabont's success is every screenwriter's dream--a mixture of a great project and a little bit of luck.",
      filmScripts: [
        {
          name: 'The Shawshank Redemption',
          genre: ['Drama', 'Criminal'],
          synopsis: 'In 1947, banker Andy Dufresne (Tim Robbins) is convicted of murdering his wife and her lover at his house, and sentenced to two consecutive life sentences at Shawshank State Penitentiary. Andy quickly befriends contraband smuggler Ellis "Red" Redding (Morgan Freeman), an inmate serving a life sentence. Red procures a rock hammer for Andy, allowing him to create small stone chess pieces. Red later gets him a large poster of Rita Hayworth, followed in later years by images of Marilyn Monroe and Raquel Welch. Andy works in the prison laundry, but is regularly assaulted by the "bull queer" gang "the Sisters" and their leader, Bogs (Mark Rolston)......'
        },
        {
          name: 'The Green Mile',
          genre: ['Drama', 'Fantasy'],
          synopsis: "At a Louisiana assisted-living home, elderly retiree Paul Edgecomb becomes emotional while viewing the film Top Hat. His companion Elaine becomes concerned, and Paul explains to her that the film reminded him of events that he witnessed in 1935 when he was an officer at Cold Mountain Penitentiary's death row, nicknamed \"The Green Mile.\". In 1935, Paul supervises Corrections Officers Brutus \"Brutal\" Howell, Dean Stanton, Harry Terwilliger, and Percy Wetmore, reporting to chief warden Hal Moores. Paul is introduced to John Coffey, a physically imposing but mild-mannered African American man sentenced to death after being convicted of raping and murdering two young white girls. He joins two other condemned convicts on the block: Eduard \"Del\" Delacroix and Arlen Bitterbuck, the latter of whom is the first to be executed. Percy, the nephew of the state governor's wife, demonstrates a sadistic streak but flaunts his family connections to avoid being held accountable; he is particularly abusive towards Del, breaking his fingers and killing his pet mouse Mr. Jingles...."
        }
      ]
      
    },
    {
      id: this.indexer++,
      name: 'Dean DeBlois',
      recomendation: "I already mentioned him in my introduction, so I might as well get him out of the way first. If you are a long-time reader, it’s no secret that I love Dean Deblois as a director and I definitely admire him just as much as a storyteller. Even when he made his screenwriting debut as a co-writer on Lilo & Stitch, the story possessed a few noticeable flourishes that would soon become his trademarks – a strong focus on family issues, character-driven plot turns, layered themes, and a notable talent for creating scene-stealing critters who become the face of a franchise.",
      filmScripts: [
        {
          name: 'How to Train Your Dragon',
          genre: ['Kids & family', 'Comedy'],
          synopsis: "Hiccup (Jay Baruchel) is a Norse teenager from the island of Berk, where fighting dragons is a way of life. His progressive views and weird sense of humor make him a misfit, despite the fact that his father (Gerard Butler) is chief of the clan. Tossed into dragon-fighting school, he endeavors to prove himself as a true Viking, but when he befriends an injured dragon he names Toothless, he has the chance to plot a new course for his people's future."
        },
        {
          name: 'Lilo & Stitch',
          genre: ['Kids & family', 'Comedy'],
          synopsis: "A tale of a young girl's close encounter with the galaxy's most wanted extraterrestrial. Lilo is a lonely Hawaiian girl who adopts a small ugly \"dog\" whom she names Stitch. Stitch would be the perfect pet if he weren't in reality a genetic experiment who has escaped from an alien planet and crash-landed on Earth. Through her love, faith and unwavering belief in ohana, the Hawaiian concept of family, Lilo helps unlock Stitch's heart and gives him the ability to care for someone else."
        }
      ]
      
    },
    {
      id: this.indexer++,
      name: 'Jared Bush',
      recomendation: "This man has only written two animated films to date. But it’s a testament to his strength as a screenwriter that both those movies were smash hits in a year full of smash hits. Yes, I’m talking about Jared Bush and his work on Zootopia and Moana.",
      filmScripts: [
        {
          name: 'Moana',
          genre: ['Kids & family', 'Adventure'],
          synopsis: "An adventurous teenager sails out on a daring mission to save her people. During her journey, Moana meets the once-mighty demigod Maui, who guides her in her quest to become a master way-finder. Together they sail across the open ocean on an action-packed voyage, encountering enormous monsters and impossible odds. Along the way, Moana fulfills the ancient quest of her ancestors and discovers the one thing she always sought: her own identity."
        },
        {
          name: 'Zootopia',
          genre: ['Kids & family', 'Comedy'],
          synopsis: "From the largest elephant to the smallest shrew, the city of Zootopia is a mammal metropolis where various animals live and thrive. When Judy Hopps (Ginnifer Goodwin) becomes the first rabbit to join the police force, she quickly learns how tough it is to enforce the law. Determined to prove herself, Judy jumps at the opportunity to solve a mysterious case. Unfortunately, that means working with Nick Wilde (Jason Bateman), a wily fox who makes her job even harder."
        }
      ]
      
    },
  ]

  getScenarists(){
    return of(this.scenarists);
  }
  getScenarist(id: number){
    const scenarist = this.scenarists.find( scenarist => scenarist.id === id)
    return of(scenarist)
  }

  getFilm(i:any){
    const scenarist = this.scenarists.find( scenarist => scenarist.id === +i.id);
    const film = scenarist?.filmScripts?.filter( film => film.name === i.filmDetails )
    return of(...film)
  }

}
