import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FilmDetailsComponent } from './components/scenarists/scenaristDetails/filmDetails/filmDetails.component';
import { HomeComponent } from './components/home/home.component';
import { ScenaristDetailsComponent } from './components/scenarists/scenaristDetails/scenaristDetails.component';
import { ScenaristsComponent } from './components/scenarists/scenarists.component';
import { ScenaristsListComponent } from './components/scenarists/scenaristsList/scenaristsList.component';
import { CompaniesComponent } from './components/companies/companies.component';
import { CompaniesListComponent } from './components/companies/companiesList/companiesList.component';
import { CompaniesDetailsComponent } from './components/companies/companiesDetails/companiesDetails.component';
import { CompanyFilmDetailsComponent } from './components/companies/companiesDetails/companyFilmDetails/companyFilmDetails.component';
import { AuthGuardService } from './services/authGuard.service';
import { LoginService } from './services/login.service';
import { ChildrenGuardService } from './services/childrenGuard.service';
import { NotFoundComponent } from './components/notFound/notFound.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ScenaristsComponent,
    ScenaristsListComponent,
    ScenaristDetailsComponent,
    FilmDetailsComponent,
    CompaniesComponent,
    CompaniesListComponent,
    CompaniesDetailsComponent,
    CompanyFilmDetailsComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    
  ],
  providers: [AuthGuardService, LoginService, ChildrenGuardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
