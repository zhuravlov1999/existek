import { Component, ElementRef, EventEmitter, Input, Output, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { INode } from '../app.component';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent {
  @Input() listItem!: INode;
  @Input() nodes!: any|INode[];
  @Input() randomId!: (any);
  @Input() bubbleDown!: string;
  
  @Output() message: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('itemRef') itemRef!: ElementRef;
  @ViewChildren(ListComponent) appRef!: QueryList<ElementRef>;

  
  sendToParent(str:string, obj:any):any{
    setTimeout(()=>{
      this.message.emit({message:str, obj: obj})
      this.itemRef.nativeElement.style.backgroundColor = str;
      this.listItem.completed = true;
      setTimeout(()=>{
        this.itemRef.nativeElement.style.backgroundColor = null;
      },1000)
    }, 1000)
    

  }
  receiveFromChild(event:any){
    this.sendToParent(event.message, event.obj) 
    console.log(event)
    
  }

  sendToChild(){
    setTimeout(()=>{
      this.itemRef.nativeElement.style.backgroundColor = this.bubbleDown;
      setTimeout(()=>{
        this.itemRef.nativeElement.style.backgroundColor = null;
      },1000)
    },1000)

    setTimeout(()=> {
      this.appRef.forEach(arg => {
        (arg as any as ListComponent).sendToChild()
      })
    }, 1000)
  }
  
  value = '';
  selectedTreeItem:any = '';
  inputEditActive = false;
  inputAddChildActive = false;
  
  editButton(item:any){
    this.selectedTreeItem = item;
    this.inputEditActive = !this.inputEditActive;
    console.log(item)
  }
  
  addChildButton(item:any){
    this.selectedTreeItem = item;
    this.inputAddChildActive = !this.inputAddChildActive;
  }


  editItem(event:Event, item:any):void{
    this.value = (<HTMLOutputElement>event.target).value
    item.name = this.value
    this.inputEditActive = false;
  }

  

  deleteItem(node:any, item:any):void{
    node.forEach((currNode:any,index:number)=>{
      if(currNode==item) node.splice(index,1);
    })
  }


  addChild(event:Event, item: any){
    this.value = (<HTMLOutputElement>event.target).value
    if(item.children === undefined){
      item.children = [{id: this.randomId(), name: this.value, completed: false}]
      console.log(item.children)
    } else {
      console.log(item)
      item.children.push({id: this.randomId(), name: this.value, completed: false})
    }
    this.inputAddChildActive = false;
  }
  

}
