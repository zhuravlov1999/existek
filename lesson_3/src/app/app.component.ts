import { Component, ElementRef, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { ListComponent } from './list/list.component';


export interface INode {
  id:string;
  name:string;
  completed: boolean;
  children?:INode[];
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  bubbleDown = '';
  text = '';
  value = '';

  @ViewChildren(ListComponent) appRef!: QueryList<ElementRef>;
  @ViewChild('itemRef') itemRef!: ElementRef;
  
  randomId():string{
    return (Math.random() + 1).toString(16).slice(2, 5);
  }

  
  receiveFromChild(event:any){
    this.bubbleDown = event.message;

    setTimeout(()=> {
      this.appRef.forEach(arg => {
        (arg as any as ListComponent).sendToChild()
      })
    }, 1000)
  }


  nodes:INode[] = [
    {id: this.randomId(), name: '1', completed: false,children:[
       {id: this.randomId(), name: '1.1', completed: false, children:[
          {id: this.randomId(), name: '1.1.1', completed: false, children:[
              {id: this.randomId(), name: '1.1.1', completed: false},
              {id: this.randomId(), name: '1.1.1', completed: false},
              {id: this.randomId(), name: '1.1.1', completed: false, children:[
                {id: this.randomId(), name: '2.1.1', completed: false,}]
              }
            ]
          }
        ]
      },
        
    {id: this.randomId(), name: '1.2', completed: false},
    {id: this.randomId(), name: '1.2', completed: false}]
    },
    {id: this.randomId(), name: '2', completed: false, children:[
        {id: this.randomId(), name: '2.1', completed: false, children:[
           {id: this.randomId(), name: '2.1.1', completed: false,}
          ]
         }
        ]
     }
  ]

  addItem(event: Event):void{
    this.value = (<HTMLOutputElement>event.target).value
    this.nodes.push({id: this.randomId(), name: this.value, completed: false})
    this.text = '';
  }
  
 

}
